<?php

namespace App\Http\Controllers;

use App\Models\Books;
use App\Models\Rent;
use App\Models\Buy;
use Illuminate\Http\Request;
use App\Http\Requests\CreateRequest;

class BooksController extends Controller
{
    public function addbook(){
        return view('addbook');
    }
  
     public function create(CreateRequest $input)
        {
        
            $store['book_name'] = $input['book_name'];
            $store['author_name'] = $input['author_name'];
            $store['price'] = $input['price'];
            $dbStatus = Books::create($store); 
        
            if ($dbStatus) {
                    $message = 'book added succesfully';
                    
                } else {
                    $message = 'Error message';
                }
        
                return redirect()->route('books.show');
            
        }
        


        
    public function showContacts()
    {
        $books = Books::all(); // Retrieve contacts from the database

        return view('show', ['books' => $books]);
        
    }


public function rent(Request $request){
      // Validate the request data
      $request->validate([
        'book_id' => 'required|integer|exists:books,id',
    ]);

    $bookId = $request->input('book_id');
    
    // Retrieve the book's price from the "books" table
    $book = Books::find($bookId);

    // Check if the book exists
    if (!$book) {
        return response()->json(['message' => 'Book not found'], 404);
    }

    $rentalAmount = intval($book->price / 6);
    
    $rents = new Rent();
    $rents->books_id = $bookId;
    $rents->rent = $rentalAmount;
    $rents->save();

    return view('rental', ['rent' => $rents, 'book' => $book]);
    
}

public function Buy(Request $request){
        $request->validate([
          'book_id' => 'required|integer|exists:books,id',
      ]);
  
      $bookId = $request->input('book_id');
      
      // Retrieve the book's price from the "books" table
      $book = Books::find($bookId);
  
      // Check if the book exists
      if (!$book) {
          return response()->json(['message' => 'Book not found'], 404);
      }
  
      // Calculate the rental amount (price divided by 6)
      $totalamount = intval($book->price + 40);
      // Store the book ID and rental amount in the "rents" table
      $buys = new Buy();
      $buys->books_id = $bookId;
      $buys->total_price = $totalamount;
  
      $buys->save();

    return view('buy', ['buy' => $buys, 'book' => $book]);
  
    
  }
}




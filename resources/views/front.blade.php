<!DOCTYPE html>
<html>
    <head lang="en">
        <title>
            Shreeman Library
        </title>   
        <meta name="viewport" content="width=device-width, initial-scale=1">

           <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
            <link rel="stylesheet" type="text/css" href="{{ ('css/front.css') }}">

    </head>




    <body>
        <!-- code for logo -->
        <div class="container-fluid " style="background-color: red;">
                    <div class="col-6">
                            <b><span class="logo">SHREEMAN</span></b>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid " style="background-color: white;">

            <!-- code for slide show -->
            <div class="slideshow-container">
                <div class="mySlides">
                    <a href="/books">
                    <img src="{{ ('image/book-store-slide-1.jpg') }}"  style="width: 1800px; height: 700px">
                    </a>
                </div>

                <div class="mySlides">
                    <a href="/addbook">
                    <img src="{{ ('image/book-store-slide-2.webp') }}" style="width: 1800px; height: 700px">
                    </a>
                </div>

                <div class="mySlides">
                    <a href="/books">
                    <img src="{{ ('image/book-store-slide-3.jpg') }}" style="width: 1800px; height: 700px">
                    </a>
                </div>
            </div>

            <div style="text-align:center">
                <span class="dot" onclick="currentSlide(1)"></span>
                <span class="dot" onclick="currentSlide(2)"></span>
                <span class="dot" onclick="currentSlide(3)"></span>
            </div> 
            <script>
                let slideIndex = 0;
                showSlides();
                
                function showSlides() {
                let i;
                let slides = document.getElementsByClassName("mySlides");
                let dots = document.getElementsByClassName("dot");
                for (i = 0; i < slides.length; i++) {
                    slides[i].style.display = "none";  
                }
                slideIndex++;
                if (slideIndex > slides.length) {slideIndex = 1}    
                for (i = 0; i < dots.length; i++) {
                    dots[i].className = dots[i].className.replace(" active", "");
                }
                slides[slideIndex-1].style.display = "block";  
                dots[slideIndex-1].className += " active";
                setTimeout(showSlides, 2000); 
                }
            </script>
        </div>
        <div class="container-fluid">
            <div class="button">
                <div class="row">
                    <div class="col-sm-12">
                       <a href="/books" class="rent-btn" > <button class="float-start" type="submit" name="RENT BOOK"> <p class=" rent-btnn"style="font-size: 20px; color: blue; font-weight:bold; margin-left:100px; margin-right:100px; margin-top:10px;">To rent a book</p></button></a>
                       
                       <a href="/addbook" > <button class="addbook" type="submit" name="ADD BOOK"> <p style="font-size: 20px; color: blue; font-weight:bold; margin-left:100px; margin-right:100px; margin-top:10px;">To Add a Book</p></button></a>

                       <a href="/books" >   <button class="float-end" type="submit" name="BUY BOOK"><p class="buy-btn" style="font-size: 20px; color: blue;font-weight:bold; margin-left:100px; margin-right:100px;  margin-top:10px;">To buy a book</p></button></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid " style="background: rgb(87, 190, 196); ">
            <div class="contact">
                            Contact Us
                            <div class="info" style="text-align: left;">
                                <pre>
                                Whatsapp No. - 123456789
                                Follow us - www.instagram.com/yogeshri
                                Call us - 1234567890
                                If you have any queries - abc.123@gmail.com
                                If you have any suggestions please tell us 
                                We always Appriciate and thank new talents<img src="{{ ('image/thumb-removebg-preview.jpg') }}" height="30px" width="30px">
                                </pre>
                            </div>
            </div>
        </div>
    
    </body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Book List</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">

<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
 <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" type="text/css" href="{{ ('css/rent.css') }}">
</head>
<body>
<div class="container-fluid " style="background-color: red;">
                    <div class="col-6">
                            <b><span class="logo">SHREEMAN</span></b>
                    </div>
                </div>
            </div>
        </div>
        <table >
  <thead>
            <tr>
                <th>Book Name</th>
                <th>Author Name</th>
                <th>Price</th>
                <th>Rent</th>
                <th>Buy</th>
            </tr>
        </thead>
        <tbody>
            @foreach($books as $book)
                <tr>
                    <td>{{ $book->book_name }}</td>
                    <td>{{ $book->author_name }}</td>
                    <td>{{ $book->price}}</td>
                    <td>
                        <!-- Each book has its own form for the rent action -->
                        <form  action="{{ route('books.rent') }}" method="post">
                            @csrf 
                            <input type="hidden" name="book_id" value="{{ $book->id }}">
                            <button class="rent-btn" type="submit">RENT</button>
                        </form>
                    </td>
                    <td>
                        <form action="{{ route('books.buy') }}" method="post">
                            @csrf <!-- Include the CSRF token -->
                            <input type="hidden" name="book_id" value="{{ $book->id }}">
                            <button class="buy-btn" type="submit">  BUY</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>

<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BooksController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/front', function () {
    return view('front');
});
Route::get('/rent', function () {
    return view('rent');
});
Route::get('/buy', function () {
    return view('buy');
});
Route::get('/rental', function () {
    return view('rental');
})->name('books.rental');
Route::get('/buy', function () {
    return view('buy');
});
Route::get('/show', function () {
    return view('show');
});
Route::get('/addbook', [BooksController::class, 'addbook'])->name('books.addbook');

Route::post('/create', [BooksController::class, 'create']);

Route::get('/books', [BooksController::class, 'showContacts'])->name('books.show');

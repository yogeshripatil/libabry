<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BooksController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('/create', [BooksController::class, 'create'])->name('books.create');
Route::get('/getrecordsid', [BooksController::class, 'getrecordsid']);
Route::get('/books',[BooksController::class, 'books'] );
Route::post('/rent',[BooksController::class, 'rent'] )->name("books.rent");

Route::post('/buy',[BooksController::class, 'Buy'] )->name("books.buy");
